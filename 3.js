let claims = require('fs').readFileSync('./3.txt').toString().split('\n')

claims = claims.map(claim => {
  return {
    id: claim.split(' ')[0].substr(1),
    x: parseInt(claim.split(' ')[2].split(',')[0]),
    y: parseInt(claim.split(' ')[2].split(',')[1]),
    w: parseInt(claim.split(' ')[3].split('x')[0]),
    h: parseInt(claim.split(' ')[3].split('x')[1])
  }
})

let fabric = {}

claims.forEach(claim => {
  for(let col=0; col<claim.w; col++){
    for(let row=0; row<claim.h; row++){
      const sqId = (col+claim.x)+'x'+(row+claim.y)
      if(!fabric[sqId]){
        fabric[sqId] = [claim.id]
      } else {
        fabric[sqId].push(claim.id)
      }
    }
  }
})

claims.forEach(claim => {
  let overlaps = false
  for(let col=0; col<claim.w; col++){
    for(let row=0; row<claim.h; row++){
      const sqId = (col+claim.x)+'x'+(row+claim.y)
      if(fabric[sqId].length>1){
        overlaps = true
      }
    }
  }
  claim.overlaps = overlaps
})

let overlapsCount = 0
Object.values(fabric).forEach(sq => {
  if(sq.length>1) overlapsCount++
})

// Part one answer:
console.log(overlapsCount)

// Part two answer:
console.log(claims.filter(claim => !claim.overlaps)[0].id)

