const _ = require('lodash')
const testPoints = `
1, 1
1, 6
8, 3
3, 4
5, 5
8, 9
`
// const points = require('fs').readFileSync('./6.txt').toString().split('\n')
const points = testPoints.split('\n')
.map(point =>{
    return {
      id: point,
      x: parseInt(point.split(',')[0]),
      y: parseInt(point.split(',')[1])
    }
  })

// Bounds:

// const minX = 41
// const minY = 58
// const maxX = 355
// const maxY = 341


const minX = 1
const minY = 1
const maxX = 8
const maxY = 9

const manDist = (cola,rowa,colb,rowb) => {
  return Math.abs(cola-colb) + Math.abs(rowa-rowb)
}

let grid = {}

for (let col=minX; col<maxX; col++){
  for (let row=minY; row<maxY; row++){
    let distances = []
    points.forEach(point=> {
      distances.push({
        dist: manDist(col,row,point.y,point.x),
        id: point.id
      })
    })
    let closest = _.minBy(distances, 'dist')
    if(distances.filter(point=> point.dist == closest.dist).length > 2) {
      // equal closest distance
      grid[col+','+row] = false
    } else {
      grid[col+','+row] = closest.id
    }
  }
}

let counts = {}
_.forEach(grid, closest => {
  if(!counts[closest]){
    counts[closest] = 1
  } else {
    counts[closest] += 1
  }
})
console.log(counts)
console.log(_.sortBy(counts))
