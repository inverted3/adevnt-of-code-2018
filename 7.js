const _ = require('lodash')
const input = require('fs').readFileSync('./7.txt').toString().split('\n')
  .map(line => {
    return {
      name: line.split(' ')[7],
      required: line.split(' ')[1]
    }
  })

let steps = {}

for (step of input){
  if(!steps[step.name]){
    steps[step.name] = {
      name: step.name,
      prereqs: [],
      done: false
    }
  }
  if(!steps[step.required]){
    steps[step.required] = {
      name: step.required,
      prereqs: [],
      done: false
    }
  }
}

for (step of input){
  steps[step.name].prereqs.push(step.required)
}

let availableSteps = (steps) => {
  return _.sortBy(Object.values(steps).filter(step => !step.done && !step.prereqs.length), ['name'])
}

let order = []
while(availableSteps(steps).length){
  let stepToComplete = availableSteps(steps)[0].name
  steps[stepToComplete].done = true
  order.push(stepToComplete)
  _.forEach(steps, step => {
    step.prereqs = step.prereqs.filter(req => req != stepToComplete)
  })
}

// Part one answer: 

console.log(order.join(''))

steps = {}

const timeTakes = stepName => {
  return stepName.charCodeAt(0)-64+60
}

for (step of input){
  if(!steps[step.name]){
    steps[step.name] = {
      name: step.name,
      prereqs: [],
      inProgress: false,
      timeLeft: timeTakes(step.name)
    }
  }
  if(!steps[step.required]){
    steps[step.required] = {
      name: step.required,
      prereqs: [],
      inProgress: false,
      timeLeft: timeTakes(step.required)
    }
  }
}

for (step of input){
  steps[step.name].prereqs.push(step.required)
}

availableSteps = (steps) => {
  return _.sortBy(Object.values(steps).filter(step => !step.inProgress && step.timeLeft>0 && !step.prereqs.length), ['name'])
}

let workers = []

for (let i=0; i<5; i++){
  workers.push({
    id: i,
    idle: true,
    task: false
  })
}

const availableWorkers = (workers) => {
  return workers.filter(worker => worker.idle)
}

let clock = 0

const tick = () => {
  console.log('tick')
  clock++
  while(availableSteps(steps).length && availableWorkers(workers).length){
    let theWorker = availableWorkers(workers)[0]
    theWorker.idle = false
    theWorker.task = availableSteps(steps)[0].name
    availableSteps(steps)[0].inProgress = true
  }
  _.forEach(steps, step => {
    if(step.inProgress){
      step.timeLeft -=1
    }
    if(step.timeLeft == 0){
      console.log('finished', step.name, clock)
      step.inProgress = false
      _.forEach(steps, reqStep => {
        reqStep.prereqs = reqStep.prereqs.filter(req => req != step.name)
      })
      let theWorker = workers.find(worker => worker.task == step.name)
      theWorker.idle = true
      theWorker.task = false
    }
  })
}

while(availableSteps(steps)){
  tick()
}