const fs = require('fs')

const ids = fs.readFileSync('./2.txt').toString().split('\n')

let twos = 0
let threes = 0

ids.forEach(id => {
  let letters = id.split('')
  let seen = {}
  letters.forEach(letter => {
    if(seen[letter]){
      seen[letter] += 1
    } else {
      seen[letter] = 1
    }
  })
  let twos_here = false
  let threes_here = false
  Object.values(seen).forEach(letter => {
    if (letter ==2 ) twos_here = true
    if (letter ==3 ) threes_here = true
  })
  if (twos_here) twos++
  if (threes_here) threes++
})

// Part one output:
console.log(twos * threes)

const differentLetters = (a,b) => {
  let result = 0
  let aLetters = a.split('')
  let bLetters = b.split('')
  for (let i=0; i<aLetters.length; i++){
    if(aLetters[i] != bLetters[i]) result++
  }
  return result
}

const commonLetters = (a,b) => {
  let result = []
  let aLetters = a.split('')
  let bLetters = b.split('')
  for (let i=0; i<aLetters.length; i++){
    if(aLetters[i] == bLetters[i]) result.push(aLetters[i])
  }
  return result.join('')
}


for (let i=0; i<ids.length; i++){
  for (let j=0; j<ids.length; j++){
    if(differentLetters(ids[i], ids[j])==1){
      // Part two output:
      console.log(commonLetters(ids[i], ids[j]))
      process.exit()
    }
  }
}
