const numPlayers = 491
const lastMarble = 71058

let board = [0]
let currentMarblePosition = 0
let newMarbleValue = 1
let players = Array(numPlayers).fill(0)
let currentPlayer = 0

const clockwise = () => {
  if(currentMarblePosition+1==board.length) return 0
  return currentMarblePosition +1
}

const counterClockwise = () => {
  if(currentMarblePosition==0) return board.length-1
  return currentMarblePosition-1
}

const tick = () => {
  if(newMarbleValue % 23 == 0){
    players[currentPlayer] += newMarbleValue
    for (let i=0; i<7; i++){
      currentMarblePosition = counterClockwise()
    }
    let splicedMarble = board.splice(currentMarblePosition+1,1)[0]
    players[currentPlayer] += splicedMarble
  } else {
    currentMarblePosition = clockwise()
    currentMarblePosition = clockwise()
    board.splice(currentMarblePosition+1, 0, newMarbleValue)
  }
  newMarbleValue++
  currentPlayer++
  if(currentPlayer==numPlayers){currentPlayer = 0}
}

while(newMarbleValue<=lastMarble){
  tick()
  if(newMarbleValue%100000){
    process.stdout.clearLine()
    process.stdout.cursorTo(0)
    let output = (newMarbleValue / lastMarble * 100) + ''
    process.stdout.write(output)
  }
}

console.log(players.reduce((acc,cur)=> {
  if(cur>acc) return cur
  return acc
},0))
