let input = require('fs').readFileSync('./5.txt').toString().split('')

const isMatch = (a,b) => {
  if(a==b) return false
  if(a.toLowerCase()== b) return true
  if(b.toLowerCase()==a) return true
  return false
}

const reactions = (str) => {
  let matches = [] // indexes to remove
  for (let i=0; i<str.length-1; i++){
    if(isMatch(str[i],str[i+1])){
      matches.push(i)
      matches.push(i+1)
      i++
    }
  }
  return matches
}

const lengthAfterReactions = (str) => {
  let reactedStr = str.slice()
  while(reactions(reactedStr).length){
    // console.log(reactedStr.length, 'reactedStr length')
    // console.log(reactions(reactedStr).length, 'reactions')
    let matches = reactions(reactedStr)
    for (let i=matches.length-1; i>=0; i--){
      reactedStr.splice(matches[i], 1)
    }
  }
  return reactedStr.length
}

// Part one:
console.log(lengthAfterReactions(input), 'input length after all reactions')

let unitTypes = 'abcdefghijklmnopqrstuvwxyz'.split('')

let mininmalUnitSize = input.length

for (let unit of unitTypes){
  let filteredInput = input.slice().filter(u => u != unit && u != unit.toUpperCase())
  let reactedFilteredInput = lengthAfterReactions(filteredInput)
  if(reactedFilteredInput<mininmalUnitSize){
    mininmalUnitSize = reactedFilteredInput
  }
}
// Part two:
console.log(mininmalUnitSize, 'minimal length after removing the best unit')