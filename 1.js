const fs = require('fs')

const input = fs.readFileSync('./1.txt').toString().split('\n')
  .map(line => parseInt(line))

let currentFreq = 0

input.forEach(change => {
  currentFreq += change
})

// Part one result:

console.log('Part one:', currentFreq)

currentFreq = 0

let visitedFrequencies = [0]

// Part two result:

let currentPos = 0
let loops = 0
while (true){
  currentFreq += input[currentPos]
  
  if(visitedFrequencies.indexOf(currentFreq) != -1){
    console.log('Part two:',currentFreq)
    process.exit()
  }
  
  visitedFrequencies.push(currentFreq)
  
  currentPos++

  if(currentPos> input.length-1) {
    currentPos=0
    loops++
  }
}

