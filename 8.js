const input = require('fs').readFileSync('./8.txt').toString().split(' ')

let pointer = 0
let totalMetas = 0

const parseNodes = node => {
  const header = {
    childNodesCount: parseInt(node[pointer]),
    metaDataCount: parseInt(node[pointer+1])
  }

  let children = []
  let metaData = []

  pointer += 2
  
  for (let i=0; i<header.childNodesCount; i++){
    children.push(parseNodes(node))
  }
  
  metaData = node.slice(pointer,pointer+header.metaDataCount)
  metaData = metaData.map(item => parseInt(item))
  metaData.forEach(item => {
    totalMetas += parseInt(item)
  })

  pointer += header.metaDataCount

  return {
    children,
    metaData
  }
}

let tree = parseNodes(input)

//Part one:
console.log(totalMetas)

const getValue = (node) => {

  if(!node.children.length) {
    return node.metaData.reduce((acc,cur) => {
      return acc+cur
    },0)
  }
  
  return node.metaData.reduce((acc,pointer) => {
    if(node.children[pointer-1]){
      return acc += getValue(node.children[pointer-1])
    }
    return acc
  },0)
}

//Part two:
console.log(getValue(tree))