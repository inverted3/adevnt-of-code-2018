let board = require('fs').readFileSync('./18.txt').toString().split('\n').map(line => line.split(''))

const adjacents = (board, row,col) => {
  const cellsToCheck = [
    board[row][col-1],
    board[row][col+1],
    board[row-1] ? board[row-1][col] : undefined,
    board[row+1] ? board[row+1][col] : undefined,
    board[row-1] ? board[row-1][col-1] : undefined,
    board[row+1] ? board[row+1][col+1] : undefined,
    board[row-1] ? board[row-1][col+1] : undefined,
    board[row+1] ? board[row+1][col-1] : undefined
  ]
  return cellsToCheck.filter(cell => cell)
}

const countTypes = (arr) => {
  return arr.reduce((acc,cur) => {
    if(acc[cur]){
      acc[cur] += 1
    } else {
      acc[cur] = 1
    }
    return acc
  }, {})
}

const nextStep = (board) => {
  let result = []
  for (let row = 0; row<board.length; row++){
    result.push([])
    for (let col=0; col<board[0].length; col++){
      let surroundings = countTypes(adjacents(board, row,col))
      if(board[row][col] == '.' && surroundings['|']>2){
        result[row].push('|')
      } else if(board[row][col] == '|' && surroundings['#']>2){
        result[row].push('#')
      } else if(board[row][col] == '#'){
        if(surroundings['#']>0 && surroundings['|']>0){
          result[row].push('#')
        } else {
          result[row].push('.')
        }
      } else {
        result[row].push(board[row][col])
      }
    }
  }
  return result
}

let seenBoards = []

for (let i=0; i<100; i++){
  if(seenBoards.includes(board)){
    console.log('already seen',seenBoards.indexOf(board))
  }
  console.log(seenBoards.map(sb => countTypes([].concat.apply([],sb))))
  seenBoards.push(board)
  if(i%100==0){
    console.log(i)
  }
  board = nextStep(board)
}
const finalTypes = countTypes([].concat.apply([],board))
console.log(finalTypes['|']*finalTypes['#'])
