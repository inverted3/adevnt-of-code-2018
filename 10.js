const lines = require('fs').readFileSync('./10.txt').toString().split('\n')

points = []

lines.forEach(line =>{
  let cleaned = line.match(/<(.*?)>/g)
  let coords = cleaned[0].replace('<','').replace('>','').split(',')
  let vels = cleaned[1].replace('<','').replace('>','').split(',')
  points.push({
    x: parseInt(coords[0]),
    y: parseInt(coords[1]),
    xVel: parseInt(vels[0]),
    yVel: parseInt(vels[1])
  })
})

const tick = () => {
  points.forEach(point => {
    point.x += point.xVel
    point.y += point.yVel
  })
}

const bounds = (points) => {
  let maxX = points.reduce((acc,cur) => {
    if(cur.x>acc) return cur.x
    return acc
  },0)
  let minX = points.reduce((acc,cur) => {
    if(cur.x<acc) return cur.x
    return acc
  },9999999)
  let maxY = points.reduce((acc,cur) => {
    if(cur.y>acc) return cur.y
    return acc
  },0)
  let minY = points.reduce((acc,cur) => {
    if(cur.y<acc) return cur.y
    return acc
  },9999999)
  return [minX,maxX,minY, maxY]
}

const size = (bounds) => {
  return (bounds[1]-bounds[0])*(bounds[3]-bounds[2])
}

let secs = 0
let lastSize = 999999999999999
while(true){
  tick()
  secs++
  if(size(bounds(points))>lastSize){
    console.log(secs)
    break
  }
  lastSize = size(bounds(points))
  // console.log(bounds(points), secs, size(bounds(points)))
}

const print = (points) => {
  let screen = Array(400).fill(Array(400).fill('.'))
  // console.log(screen.map(row=>row.join('')).join('\n'))
  points.forEach(point =>{
    // console.log(point)
    if(screen[point.y] && screen[point.y][point.x]){
      screen[point.x][point.y] = '#'
    }
  })
  console.log(screen.map(row=>row.join('')))
}
print(points)