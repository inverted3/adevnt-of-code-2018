const _ = require('lodash')

let lines = require('fs').readFileSync('./4.txt').toString().split('\n')
lines.sort()

let guards = {}

let currentGuard

for (let i=0; i<lines.length; i++){
  if(lines[i].indexOf('Guard') != -1){
    currentGuard = lines[i].split('#')[1].split(' ')[0]
    if(!guards[currentGuard]){
      guards[currentGuard] = {
        id:currentGuard
      }
    }
  } else {
    const startMinute = parseInt(lines[i].substr(15,2))
    const endMinute = parseInt(lines[i+1].substr(15,2))
    if(!guards[currentGuard].sleepTotal){
      guards[currentGuard].sleepTotal = 0
    }
    guards[currentGuard].sleepTotal += endMinute-startMinute

    for (let minute = startMinute; minute<endMinute; minute++){
      if(!guards[currentGuard].minutes){
        guards[currentGuard].minutes = {}
      }
      if(!guards[currentGuard].minutes[minute]){
        guards[currentGuard].minutes[minute] = {
          minute: minute,
          slept:0
        }
      }
      guards[currentGuard].minutes[minute].slept += 1
    }
    i++
  }
}

const chosenGuard = _.sortBy(guards, ['sleepTotal']).reverse().filter(guard => guard.sleepTotal)[0]
const mostSleptMinute = _.sortBy(chosenGuard.minutes, ['slept']).reverse()[0]

// Part one result:

console.log('id:', chosenGuard.id, 'most slept minute:', mostSleptMinute.minute, 'result: ', parseInt(chosenGuard.id)*parseInt(mostSleptMinute.minute))

let maxSlept = 0
let guardId
let chosenMinute
for (let guard of Object.values(guards)){
  if(guard.minutes){
    const mostSleptMinute = _.sortBy(guard.minutes, ['slept']).reverse()[0]
    if(mostSleptMinute.slept > maxSlept){
      maxSlept = mostSleptMinute.slept
      guardId = guard.id
      chosenMinute = mostSleptMinute.minute
    }    
  }
}

// Part two answer:
console.log(parseInt(guardId) * parseInt(chosenMinute))